﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    class Competitor
    {
        public int Id;
        public int Age;
        public float Weight;
        public string Name;
        public Manager Coach;
    }
}
